/**************************************/
/* SCRIPT PARA INSERTAR DATOS BÁSICOS */
/**************************************/

use oauth_phishing;

-- usuarios y roles
INSERT INTO rol (id, nombre) VALUES (1, 'ADMIN');
INSERT INTO rol (id, nombre) VALUES (2, 'USER');

INSERT INTO usuario (id, nombre, fecha_creacion, email, habilitado, clave) VALUES (1, 'admin', '2019-03-05 12:00:00', 'admin@admin.com', 1, '$2a$10$YRyW2qz4S2u2KHstxrvDxuMwLSFAtYzRjHX6GfCTKIbD7fjlaK0qu');
INSERT INTO usuario (id, nombre, fecha_creacion, email, habilitado, clave) VALUES (2, 'user', '2019-03-05 12:00:00','user@user.com', 1, '$2a$10$r61h7EYELpUO71owNVwqse.R5uVQ.47okP6vZVPKCxBp4hSRkv/P.');

INSERT INTO usuario_rol (usuario_id, rol_id) VALUES (1, 1);
INSERT INTO usuario_rol (usuario_id, rol_id) VALUES (2, 2);