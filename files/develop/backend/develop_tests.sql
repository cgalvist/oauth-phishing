/*
drop database oauth_phishing;
create database oauth_phishing;
*/
use oauth_phishing;

show tables;

/***********************/
/* USUARIOS Y PERMISOS */
/***********************/

-- clave: MORE_2017_4889955	

select u.email, u.clave from usuario u;

select * from usuario;
select * from usuario u where u.email='admin@admin.com';

select * from usuario_rol;

/****************************/
/* AUTENTICACION CON OAUTH2 */
/****************************/

select * from client_details;
select * from oauth_access_token;
select * from oauth_approvals;
select * from oauth_client_details;
select * from oauth_client_token;
select * from oauth_code;
select * from oauth_refresh_token;

/*
DELETE FROM oauth_client_details where client_id = 'Oauth_Phishing_Web';

INSERT INTO oauth_client_details (client_id, access_token_validity, additional_information, authorities, authorized_grant_types, autoapprove, 
client_secret, refresh_token_validity,  resource_ids, scope, web_server_redirect_uri)
VALUES ('OAuth_Phishing_Web', NULL, '{}', '', 'password,authorization_code,refresh_token', '', '{noop}OAuth_Phishing_2019_01', NULL, '', 'read,write,openid', '');
*/

/**********************/
/* PRUEBAS (ELIMINAR) */
/**********************/