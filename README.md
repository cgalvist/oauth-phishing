# oauth-phishing

Prototipo de herramienta para obtener datos de cuentas de usuario por medio de OAuth 2.0.

El propósito de esta herramienta es generar concienciación a las personas del riesgo de aceptar autorizaciones de acceso a aplicaciones desconocidas.