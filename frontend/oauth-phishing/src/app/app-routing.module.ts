import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404PageComponent } from '@app/shared/pages/error404-page/error404-page.component';
import { HomePageComponent } from '@app/shared/pages/home-page/home-page.component';
import { AppConfig } from '@app/configs/app.config';

const routes: Routes = [

  // main page
  { path: '', redirectTo: 'index', pathMatch: 'full' },

  { 
    path: 'index', 
    component: HomePageComponent,
  },

  //fake pages
  //{ path: 'test', component: FakeLoginComponent, pathMatch: 'full' },
  { 
    path: AppConfig.routes.fake, 
    loadChildren: './modules/fake-page/fake-page.module#FakePageModule' 
  },

  //admin pages
  //{ path: 'test', component: FakeLoginComponent, pathMatch: 'full' },
  { 
    path: AppConfig.routes.admin, 
    loadChildren: './modules/admin-page/admin-page.module#AdminPageModule' 
  },

  // error 404 page
  { path: AppConfig.routes.error404, component: Error404PageComponent },

  // otherwise redirect to 404
  { path: '**', redirectTo: '/' + AppConfig.routes.error404 },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    }),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
