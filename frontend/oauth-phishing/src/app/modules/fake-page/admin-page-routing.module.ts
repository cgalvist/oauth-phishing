import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FakePageComponent } from './fake-page.component';
import { FakeLoginComponent } from '@app/modules/fake-page/pages/fake-login/fake-login.component';
import { LoginSuccessComponent } from '@app/modules/fake-page/pages/login-success/login-success.component';
import { LoginErrorComponent } from '@app/modules/fake-page/pages/login-error/login-error.component';


const fakeRoutes: Routes = [
  { 
    path: '', 
    component: FakePageComponent, 
    children: [
      { path: 'login', component: FakeLoginComponent, },
      { path: 'success', component: LoginSuccessComponent,},
      { path: 'error', component: LoginErrorComponent,},
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(fakeRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AdminPageRoutingModule { }
