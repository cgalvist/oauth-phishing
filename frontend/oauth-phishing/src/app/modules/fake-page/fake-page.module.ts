import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule }   from '@angular/forms';
import { AdminPageRoutingModule } from './admin-page-routing.module';

import { FakeLoginComponent } from './pages/fake-login/fake-login.component';
import { LoginSuccessComponent } from './pages/login-success/login-success.component';
import { LoginErrorComponent } from './pages/login-error/login-error.component';
import { FakePageComponent } from './fake-page.component';

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    AdminPageRoutingModule,
  ],
  declarations: [
    FakeLoginComponent,
    LoginSuccessComponent,
    LoginErrorComponent,
    FakePageComponent
  ],
  entryComponents: [
    //HeroRemoveComponent
  ]
})
export class FakePageModule { }
