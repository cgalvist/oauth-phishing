import { Component, OnInit } from '@angular/core';
import { Constants } from '@app/shared/util/app.constants';

@Component({
  selector: 'app-fake-login',
  templateUrl: './fake-login.component.html',
  styleUrls: ['./fake-login.component.css']
})
export class FakeLoginComponent implements OnInit {

  private actionUrl: string;

  constructor() {
    this.actionUrl = Constants.BACK_END_URL;
  }

  ngOnInit() {
  }

  /**
  * log in service (with external OAuth 2.0 server)
  */
  public authUrl(provider: string): string {
    return `${this.actionUrl}oauth2/authorize/${provider}`;
  }

}
