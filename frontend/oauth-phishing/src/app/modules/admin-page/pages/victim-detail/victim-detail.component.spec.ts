import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VictimDetailComponent } from './victim-detail.component';

describe('VictimDetailComponent', () => {
  let component: VictimDetailComponent;
  let fixture: ComponentFixture<VictimDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VictimDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VictimDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
