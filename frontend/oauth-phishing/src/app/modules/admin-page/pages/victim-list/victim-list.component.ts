import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import { DataService } from '@app/shared/services/data.service';
import { Victim } from '@app/modules/admin-page/shared/victim.model';

@Component({
  selector: 'app-victim-list',
  templateUrl: './victim-list.component.html',
  styleUrls: ['./victim-list.component.css'],
})
export class VictimListComponent implements OnInit {

  displayedColumns:string[];
  dataSource:any = new MatTableDataSource<Victim>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private _service: DataService,
  ) {
    this.displayedColumns = ['creationDate', 'editionDate', 'email', 'name', 'actions', ];
    //this.dataSource = new MatTableDataSource<Victim>(ELEMENT_DATA);
    this.getData();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  /**
   * get victims list from our back end
   */
  getData() {
    this._service.get("victim/getAll", null).subscribe(
      (data: Victim[]) => {
        this.dataSource = new MatTableDataSource<Victim>(data);
      }
    );
  }

}
