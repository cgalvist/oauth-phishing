export class Mail {
    createdDateTime: string;
    lastModifiedDatetime: string;
    receiveDateTime: string;
    hasAttachments: string;
    subject: string;
    bodyPreview: string;
}