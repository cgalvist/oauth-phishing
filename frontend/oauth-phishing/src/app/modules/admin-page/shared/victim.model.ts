export class Victim {
    email: string;
    name: string;
    accessToken: string;
    creationDate: Date;
    editionDate: Date;
}