import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminPageComponent } from './admin-page.component';
import { VictimListComponent } from './pages/victim-list/victim-list.component';
import { VictimDetailComponent } from './pages/victim-detail/victim-detail.component';


const adminRoutes: Routes = [
  { 
    path: '', component: AdminPageComponent,
    children: [
      { 
        path: 'victims', 
        component: VictimListComponent,
        children: [
          { path: ':id', component: VictimDetailComponent },
        ]
      },
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(adminRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AdminPageRoutingModule { }
