import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule }   from '@angular/forms';
import { AdminPageRoutingModule } from './admin-page-routing.module';

import { AdminPageComponent } from './admin-page.component';
import { VictimListComponent } from './pages/victim-list/victim-list.component';
import { VictimDetailComponent, VictimDetailDialog } from './pages/victim-detail/victim-detail.component';

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    AdminPageRoutingModule,
  ],
  declarations: [
    AdminPageComponent,
    VictimListComponent,
    VictimDetailComponent,
    VictimDetailDialog,
  ],
  entryComponents: [
    VictimDetailDialog,
  ]
})
export class AdminPageModule { }
