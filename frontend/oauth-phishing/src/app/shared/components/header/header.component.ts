import { Component, Inject, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {APP_CONFIG, AppConfig} from '../../../configs/app.config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  language:string = 'en';
  appConfig: any;
  menuItems: any[];
  currentLang: string;

  constructor(
    @Inject(APP_CONFIG) appConfig: any,
    private translateService: TranslateService,
  ) {
    this.appConfig = appConfig;
  }

  ngOnInit() {
    this.currentLang = this.translateService.currentLang;
    this.loadMenus();
  }

  changeLanguage(language: string): void {
    this.translateService.use(language).subscribe(() => {
      this.loadMenus();
      this.language = language;
    });
  }

  private loadMenus(): void {
    this.menuItems = [
      { link: '/', name: ('home') },
      { link: '/fuser/login', name: ('login') }
    ];
  }

}
