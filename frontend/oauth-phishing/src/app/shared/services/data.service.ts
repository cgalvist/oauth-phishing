import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators'

import { Constants } from '@app/shared/util/app.constants';

//import { AuthenticationService } from '@app/services/authentication/authentication.service';

/**
 * general REST service
 */
@Injectable()
export class DataService {
    private actionUrl: string;    
    private headersEmpty: HttpHeaders;
    private headersGet: HttpHeaders;
    private headersPost: HttpHeaders;
    private headersPostFD: HttpHeaders;
    private accessToken:string;

    constructor(
        private _http: HttpClient, 
        //private _authenticationService: AuthenticationService,
    ) {
        //_authenticationService.getAccessToken().subscribe(val => this.accessToken = val);

        this.actionUrl = Constants.BACK_END_URL;

        this.headersGet = new HttpHeaders();
        if (this.accessToken) {
            this.headersGet.append('Authorization', 'Bearer ' + this.accessToken);
        }

        this.headersEmpty = this.headersGet;

        this.headersPost = this.headersGet;
        this.headersPost.append('Content-Type', 'application/json');
        this.headersPost.append('Accept', 'application/json');

        this.headersPostFD = this.headersGet;
        //this.headersPostFD.append('Content-Type', 'multipart/form-data');
    }

    /**
     * get JSON object
     */
    public get = (url: string, params: HttpParams): Observable<Object> => {
        //console.log(this.actionUrl + url);
        return this._http.get(this.actionUrl + url, {
            params: params,
        }).pipe(
            map( res => res ),
            catchError(this.handleError),
        );
    }

    /**
     * get file (download)
     */
    public getFile = (Object: object, url: string, params: HttpParams): Observable<Object> => {
        return this._http.get(this.actionUrl + url, { 
            params: params,
            headers: this.headersEmpty,
            // responseType: ResponseContentType.Blob,
            responseType: 'blob',
        })
        .pipe( 
            // map( res => res.json() ),
            map( res => res ),
            catchError(this.handleError),
        );
    }

    /**
     * save JSON object
     */
    public save = (url: string, item: Object): Observable<Object> => {
        return this._http.post(this.actionUrl + url, item, { 
            headers: this.headersGet,
        })
        .pipe( 
            // map( res => res.json() ),
            map( res => res ),
            catchError(this.handleError),
        );
    }

    /**
     * save JSON object (with GET params)
     */
    public saveWithParams = (url: string, item: Object, params: HttpParams): Observable<Object> => {
        return this._http.post(this.actionUrl + url, item, { 
            params: params,
            headers: this.headersGet,
        })
        .pipe( 
            // map( res => res.json() ),
            map( res => res ),
            catchError(this.handleError),
        );
    }

    /**
     * Form-Data request
     */
    public saveFD = (url: string, item: Object): Observable<Object> => {
        return this._http.post(this.actionUrl + url, item, { 
            headers: this.headersPostFD,
        })
        .pipe( 
            // map( res => res.json() ),
            map( res => res ),
            catchError(this.handleError),
        );
    }

    public delete = (Object: object, url: string, id: number): Observable<Object> => {
        return this._http.delete(this.actionUrl + url + id, { 
            headers: this.headersGet,
        })
        .pipe( 
            // map( res => res.json() ),
            map( res => res ),
            catchError(this.handleError),
        );
    }

    private handleError(error: HttpErrorResponse) {
        //console.error(error);
        
        // detect invalid access token
        /*
        if (this._authenticationService) {
            if (this._authenticationService.refreshShouldHappen(error)) {
                this._authenticationService.refreshToken().subscribe(
                    () => {
                        console.log("refreshing token");
                        location.reload(true);
                    },
                    error => {
                        console.log(error);
                    },
                    () => {
                        //console.log("request finished");
                    }
                );
            } else {
                this._authenticationService.clearTokens();
            }
        }
        */

        return observableThrowError(error || 'Server error');
    }
}
