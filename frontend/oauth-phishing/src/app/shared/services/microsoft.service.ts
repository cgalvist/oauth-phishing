import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators'

import { Constants } from '@app/shared/util/app.constants';

//import { AuthenticationService } from '@app/services/authentication/authentication.service';

/**
 * general REST service for Microsoft API
 */
@Injectable()
export class MicrosoftService {
    private actionUrl: string;    
    private headersEmpty: HttpHeaders;
    private headersGet: HttpHeaders;
    private headersPost: HttpHeaders;
    private headersPostFD: HttpHeaders;
    private accessToken:string;

    constructor(
        private _http: HttpClient, 
        //private _authenticationService: AuthenticationService,
    ) {
        //_authenticationService.getAccessToken().subscribe(val => this.accessToken = val);

        this.actionUrl = Constants.MICROSOFT_API_URL;

        this.headersGet = new HttpHeaders();
        if (this.accessToken) {
            this.headersGet.append('Authorization', 'Bearer ' + this.accessToken);
        }

        this.headersEmpty = this.headersGet;

        this.headersPost = this.headersGet;
        this.headersPost.append('Content-Type', 'application/json');
        this.headersPost.append('Accept', 'application/json');

        this.headersPostFD = this.headersGet;
        //this.headersPostFD.append('Content-Type', 'multipart/form-data');
    }

    /**
     * get JSON object
     */
    public get = (url: string, params: HttpParams, token: string): Observable<Object> => {
        let headersGetTemp = new HttpHeaders().append('Authorization', 'Bearer ' + token);

        return this._http.get(this.actionUrl + url, {
            params: params,
            headers: headersGetTemp,
        }).pipe(
            map( res => res ),
            catchError(this.handleError),
        );
    }

    private handleError(error: HttpErrorResponse) {
        return observableThrowError(error || 'Server error');
    }
}
