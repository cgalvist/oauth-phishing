export class Constants {

    /**
     * back end configuration
     */

    public static BACK_END_URL: string = 
        (location.hostname == 'localhost' ? 'http://localhost:8080/' : location.protocol + '//' + location.host + '/oauth-phishing/');
    
    /**
     * Microsoft graph API url
    */
    public static MICROSOFT_API_URL: string = 'https://graph.microsoft.com/v1.0/';
    
    /**
     * forms regular expressions
     */
    public static REGEX_USERNAME: string = '^[a-z0-9_-]{8,15}$';
    public static REGEX_EMAIL: string = '[a-zA-Z0-9.!#$%&’*+/=?^_\\-`{|}~-ñ]+@[a-zA-Z0-9-ñ]+(?:\\.[a-zA-Z0-9-]+)*';
    public static REGEX_PASSWORD: string = '.{6,300}';
}
