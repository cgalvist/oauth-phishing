import { NgModule } from '@angular/core';
import { MaterialModule } from './modules/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxScrollToFirstInvalidModule } from '@ismaestro/ngx-scroll-to-first-invalid';
import { DataService } from '@app/shared/services/data.service';
import { MicrosoftService } from '@app/shared/services/microsoft.service';

import { HomePageComponent } from './pages/home-page/home-page.component';
import { Error404PageComponent } from './pages/error404-page/error404-page.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    RouterModule,
    NgxScrollToFirstInvalidModule,
  ],
  declarations: [
    FooterComponent, 
    HeaderComponent, 
    HomePageComponent, 
    Error404PageComponent, SpinnerComponent
  ],
  exports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    TranslateModule,
    HeaderComponent,
    FooterComponent,
    SpinnerComponent,
    NgxScrollToFirstInvalidModule,
  ],
  providers: [
    DataService,
    MicrosoftService,
  ]
})
export class SharedModule { }
