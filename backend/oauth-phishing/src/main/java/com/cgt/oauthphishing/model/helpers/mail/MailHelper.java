package com.cgt.oauthphishing.model.helpers.mail;

/**
 * Mail class representation
 * */
public class MailHelper {
	
	private String[] recipients;
	private String subject;
	private String text;
	
	public String[] getRecipients() {
		return recipients;
	}
	public void setRecipients(String[] recipients) {
		this.recipients = recipients;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

}
