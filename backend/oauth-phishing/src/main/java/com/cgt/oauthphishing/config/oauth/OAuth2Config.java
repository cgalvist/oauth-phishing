package com.cgt.oauthphishing.config.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.cgt.oauthphishing.service.user.CustomUserDetailsService;
import com.cgt.oauthphishing.service.user.CustomUserOAuthService;

import org.springframework.jdbc.datasource.init.DataSourceInitializer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * resource and authentication servers configuration for OAuth2
 * */
@Configuration
@EnableOAuth2Client
public class OAuth2Config extends ResourceServerConfigurerAdapter {
	
	/**
	 * authentication server configuration
	 * */
	@Configuration
	@EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
		
		@Autowired
		private DataSource dataSource;
		
		@Autowired
	    @Qualifier("authenticationManagerBean")
	    private AuthenticationManager authenticationManager;
		
        @Autowired
        private CustomUserDetailsService userDetailsService;
	 
	    @Override
	    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
	    	oauthServer.allowFormAuthenticationForClients();
	    }
	 
	    @Override
	    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			clients.jdbc(dataSource)
			/*
		          .withClient("OAuth_Phishing_Web")
		          .secret("{noop}OAuth_Phishing_2019_01")
		          .authorizedGrantTypes("password","authorization_code", "refresh_token")
		          .scopes("read","write","openid")
	          .and()
		          .withClient("OAuth_Phishing_App")
		          .secret("{noop}OAuth_Phishing_2019_02")
		          .authorizedGrantTypes("password","authorization_code", "refresh_token")
		          .scopes("read","write","openid")
		    */
	        ;
	    }
	 
	    @Override
	    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
	        endpoints
	          .tokenStore(tokenStore())
	          .authenticationManager(authenticationManager)
	          .userDetailsService(userDetailsService);
	    }

	    @Qualifier("tokenStoreAuthorizationBean")
	    public TokenStore tokenStore() {
	        return new JdbcTokenStore(dataSource);
	    }
	         
	    @Bean
	    public DataSourceInitializer dataSourceInitializer(DataSource dataSource) {
	        DataSourceInitializer initializer = new DataSourceInitializer();
	        initializer.setDataSource(dataSource);
	        return initializer;
	    }
	}
	
	/**
	 * configuration for resource server
	 * */
	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
		
		@Autowired
		private DataSource dataSource;
		
		@Autowired
		private CustomUserOAuthService customUserOAuthService;
		
		@Value("${app.oauth2.redirect-uri-success}")
	    private String redirectUriSuccess;
		
		@Value("${app.oauth2.redirect-uri-error}")
	    private String redirectUriError;
		
		@Override
	    public void configure(final HttpSecurity http) throws Exception {
	        // @formatter:off
			/*
			http
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
				.and()
					.authorizeRequests()
					.anyRequest()
					.permitAll();
			*/
			/*
			http
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
		    	.and().antMatcher("/**").authorizeRequests().antMatchers("/login**","/user").permitAll()
				.and().authorizeRequests().anyRequest().permitAll();
				*/

			//TODO: delete some url services for required authentication
	    	http
		        .csrf()
			        .disable()
			        .antMatcher("/**")
			        .authorizeRequests()
			        .antMatchers("/login**", "/", /*delete this after ->*/ "/victim/*")
			        .permitAll()
			        .antMatchers("/error**")
			        .anonymous()
			        .anyRequest()
			        .authenticated()
		        .and()
		        .oauth2Login()
			        .redirectionEndpoint()
		                .baseUri("/oauth2/callback/*")
	                .and()
	                .userInfoEndpoint()
	                	.oidcUserService(customUserOAuthService)
		            .and()
	                .authorizationEndpoint()
		                .baseUri("/oauth2/authorize")
		                .authorizationRequestRepository(customAuthorizationRequestRepository())
		            .and().successHandler(customSuccessHandler());
			/*
			http
    			.authorizeRequests().anyRequest().permitAll();
			*/
			// @formatter:on
		}
		
		@Bean
		public AuthorizationRequestRepository<OAuth2AuthorizationRequest> customAuthorizationRequestRepository() {
			return new HttpSessionOAuth2AuthorizationRequestRepository();
		}
		
		/**
		 * custom success handler (for change default URL)
		 * */
		public AuthenticationSuccessHandler customSuccessHandler() {
			RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
			
			return new AuthenticationSuccessHandler() {
			    @Override
			    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			            Authentication authentication) throws IOException, ServletException {
			        redirectStrategy.sendRedirect(request, response, redirectUriSuccess);
			    }
			};
		}
		
		/**
		 * custom error handler (for change default URL)
		 * */
		public AuthenticationFailureHandler customErrorHandler() {
			RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
			
			return new AuthenticationFailureHandler() {
				@Override
				public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
						AuthenticationException exception) throws IOException, ServletException {
					redirectStrategy.sendRedirect(request, response, redirectUriError);
					
				}
			};
		}

		/**
		 * bean for add order to class filters
		 * */
		@Bean
		public FilterRegistrationBean<OAuth2ClientContextFilter> oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
			FilterRegistrationBean<OAuth2ClientContextFilter> registration = new FilterRegistrationBean<OAuth2ClientContextFilter>();
			registration.setFilter(filter);
			registration.setOrder(-100);
			return registration;
		}

	    @Override
	    public void configure(final ResourceServerSecurityConfigurer config) {
	        config.tokenServices(tokenServices());
	    }
	    
	    @Bean
	    @Primary
	    public DefaultTokenServices tokenServices() {
	        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
	        defaultTokenServices.setTokenStore(tokenStore());
	        return defaultTokenServices;
	    }

	    @Qualifier("tokenStoreResourceBean")
	    public TokenStore tokenStore() {
	        return new JdbcTokenStore(dataSource);
	    }
	}
}