package com.cgt.oauthphishing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.cgt.oauthphishing.config.properties.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class OauthPhishingApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthPhishingApplication.class, args);
	}

}

