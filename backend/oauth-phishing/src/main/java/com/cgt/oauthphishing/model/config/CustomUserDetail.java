package com.cgt.oauthphishing.model.config;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Class for read user data from Spring
 * */
public class CustomUserDetail implements UserDetails {

	private static final long serialVersionUID = 1L;
	private AuthUser authUser;
	
	List<GrantedAuthority> authorities=null;
	
	public AuthUser getUser() {
        return authUser;
    }

    public void setUser(AuthUser authUser) {
        this.authUser = authUser;
    }

    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<GrantedAuthority> authorities)
    {
        this.authorities = authorities;
    }

    public String getPassword() {
        return authUser.getPassword();
    }

    public String getUsername() {
        return authUser.getUsername();
    }

    public boolean isAccountNonExpired() {
        return authUser.isAccountNonExpired();
    }

    public boolean isAccountNonLocked() {
        return authUser.isAccountNonLocked();
    }

    public boolean isCredentialsNonExpired() {
        return authUser.isCredentialsNonExpired();
    }

    public boolean isEnabled() {
        return authUser.isEnabled();
    }

}

