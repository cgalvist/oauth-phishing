package com.cgt.oauthphishing.util.constants;

/**
 * Class with constant values
 * */
public class Constants {

	// file types
	public static final int FILE_TYPE_PDF = 1;
	public static final int FILE_TYPE_XLSX = 2;	

}
