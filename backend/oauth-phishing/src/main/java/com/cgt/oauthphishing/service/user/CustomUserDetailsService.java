package com.cgt.oauthphishing.service.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.cgt.oauthphishing.dao.user.UserRepository;
import com.cgt.oauthphishing.model.config.AuthUser;
import com.cgt.oauthphishing.model.config.CustomUserDetail;
import com.cgt.oauthphishing.model.tables.user.Role;
import com.cgt.oauthphishing.model.tables.user.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private final UserRepository usuarioRepository;

	@Autowired
	public CustomUserDetailsService(UserRepository userRepository) {
		this.usuarioRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		User user = usuarioRepository.findOneByEmail(username);
		
		if (user == null)
			throw new UsernameNotFoundException(String.format("User %s does not exist!", username));
		
		Set<Role> userRoles = user.getRoles();
		if (userRoles != null && !userRoles.isEmpty()) {
            for (Role userRole : userRoles) {
                  authorities.add(new SimpleGrantedAuthority(userRole.getName()));
            }
		}
		
		AuthUser authUser = new AuthUser(
			username, 
			user.getKey(), 
			user.getEnabled(), 
			true, 
			true, 
			true, 
			authorities, 
			user.getId(), 
			user.getName()
		);
		
		CustomUserDetail customUserDetail = new CustomUserDetail();
        customUserDetail.setUser(authUser);
        customUserDetail.setAuthorities(authorities);
		
		return customUserDetail;
	}

}