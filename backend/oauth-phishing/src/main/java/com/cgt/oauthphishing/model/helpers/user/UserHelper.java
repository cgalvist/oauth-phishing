package com.cgt.oauthphishing.model.helpers.user;

/**
 * Custom User representation for creation and edition
 * */
public class UserHelper {
	
	private Integer id;
	private String name;
	private String email;
	private boolean enabled = true;
	private String password;	
	private Integer[] rolesIds;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer[] getRolesIds() {
		return rolesIds;
	}
	public void setRolesIds(Integer[] rolesIds) {
		this.rolesIds = rolesIds;
	}
	

}
