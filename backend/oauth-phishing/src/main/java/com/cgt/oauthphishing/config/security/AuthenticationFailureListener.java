package com.cgt.oauthphishing.config.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import com.cgt.oauthphishing.dao.user.UserRepository;
import com.cgt.oauthphishing.model.tables.user.User;
import com.cgt.oauthphishing.service.security.LoginAttemptService;
import com.cgt.oauthphishing.util.Logs;

/**
 * class for detect failed attempt logins (BFD - Brute Force Detection) 
 * */
@Component
public class AuthenticationFailureListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
	
	private final Logger log = LoggerFactory.getLogger(AuthenticationFailureListener.class);
	
	@Autowired
	private Logs logs;

	@Autowired
	private UserRepository userRepository;
	
    @Autowired
    private LoginAttemptService loginAttemptService;

	public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {

		String username = event.getAuthentication().getName();

		User user = userRepository.findOneByEmail(username);
		
		if (user != null) {
			log.debug("login failed: " + user.getEmail());
			loginAttemptService.loginFailed(username);
			
			// if there are many failed attempt logins, lock user
			if(loginAttemptService.isBlocked(username)){
				log.debug("lock user: " + user.getEmail());
				
				logs.save(user.getEmail(), "Se ha bloqueado al usuario por varios intentos fallidos de inicio de sesi\u00f3n");	

				user.setEnabled(false);
				userRepository.save(user);
			}
			
		}
	}
}
