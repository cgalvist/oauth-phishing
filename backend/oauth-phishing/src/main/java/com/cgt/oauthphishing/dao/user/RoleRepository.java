package com.cgt.oauthphishing.dao.user;

import org.springframework.data.repository.CrudRepository;

import com.cgt.oauthphishing.model.tables.user.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {
	
}