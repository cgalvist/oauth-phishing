package com.cgt.oauthphishing.config.security;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import com.cgt.oauthphishing.dao.user.UserRepository;
import com.cgt.oauthphishing.model.tables.user.User;

/**
 * class for detect success login 
 * */
@Component
public class AuthenticationSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {
	
    @Autowired
    UserRepository userRepository;

    /**
     * handle data in success authentication
     * */
	public void onApplicationEvent(AuthenticationSuccessEvent event) {

		String username = event.getAuthentication().getName();
		User user = userRepository.findOneByEmail(username);
		
		if (user != null) {
			user.setLastLogin(new Date());
			userRepository.save(user);
		}		
		
	}

}
