package com.cgt.oauthphishing.util.tools;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import net.minidev.json.JSONObject;

/**
 * general tools
 * */
public class Tools {
	
	/**
	 * get standard response error
	 * @param message is the error message
	 * */
	public static ResponseEntity<JSONObject> jsonError(String message) {
		JSONObject response = new JSONObject();		
		response.put("status", "error");
		response.put("message", message);
		return new ResponseEntity<JSONObject>(response, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * get standard response error
	 * @param message is the error message
	 * @param data is additional error data as JSON object
	 * */
	public static ResponseEntity<JSONObject> jsonError(String message, JSONObject data) {
		JSONObject response = new JSONObject();		
		response.put("status", "error");
		response.put("message", message);
		response.put("data", data);
		return new ResponseEntity<JSONObject>(response, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * get standard response success
	 * @param data is the response data
	 * */
	public static ResponseEntity<JSONObject> jsonSuccess(Object data) {
		JSONObject response = new JSONObject();		
		response.put("status", "success");
		response.put("data", data);
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}

}
