package com.cgt.oauthphishing.dao.user;

import org.springframework.data.repository.CrudRepository;

import com.cgt.oauthphishing.model.tables.user.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	public User findOneByEmail(String email);
	
} 
