package com.cgt.oauthphishing.config.scheduling;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Scheduling back end configuration
 * */
@Configuration
@EnableScheduling
public class SchedulingConfig {

}
