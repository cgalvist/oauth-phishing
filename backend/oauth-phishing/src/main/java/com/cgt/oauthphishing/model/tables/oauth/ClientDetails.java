package com.cgt.oauthphishing.model.tables.oauth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ClientDetails")
public class ClientDetails {

	@Id
	@Column(name = "appId", length = 255, unique = true, nullable = false)
	private String appId;
	@Column(name = "resourceIds", length = 255)
	private String resourceIds;
	@Column(name = "appSecret", length = 255)
	private String appSecret;
	@Column(name = "scope", length = 255)
	private String scope;
	@Column(name = "grantTypes", length = 255)
	private String grantTypes;
	@Column(name = "redirectUrl", length = 255)
	private String redirectUrl;
	@Column(name = "authorities", length = 255)
	private String authorities;
	@Column(name = "access_token_validity")
	private Integer accessTokenValidity;
	@Column(name = "refresh_token_validity")
	private Integer refreshTokenValidity;
	@Column(name = "additionalInformation", length = 4096)
	private String additionalInformation;
	@Column(name = "autoApproveScopes", length = 255)
	private String autoApproveScopes;
	
	public ClientDetails(){}

	public ClientDetails(String appId, String resourceIds, String appSecret, String scope, String grantTypes,
			String redirectUrl, String authorities, Integer accessTokenValidity, Integer refreshTokenValidity,
			String additionalInformation, String autoApproveScopes) {
		super();
		this.appId = appId;
		this.resourceIds = resourceIds;
		this.appSecret = appSecret;
		this.scope = scope;
		this.grantTypes = grantTypes;
		this.redirectUrl = redirectUrl;
		this.authorities = authorities;
		this.accessTokenValidity = accessTokenValidity;
		this.refreshTokenValidity = refreshTokenValidity;
		this.additionalInformation = additionalInformation;
		this.autoApproveScopes = autoApproveScopes;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getGrantTypes() {
		return grantTypes;
	}

	public void setGrantTypes(String grantTypes) {
		this.grantTypes = grantTypes;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public Integer getAccessTokenValidity() {
		return accessTokenValidity;
	}

	public void setAccessTokenValidity(Integer accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}

	public Integer getRefreshTokenValidity() {
		return refreshTokenValidity;
	}

	public void setRefreshTokenValidity(Integer refreshTokenValidity) {
		this.refreshTokenValidity = refreshTokenValidity;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getAutoApproveScopes() {
		return autoApproveScopes;
	}

	public void setAutoApproveScopes(String autoApproveScopes) {
		this.autoApproveScopes = autoApproveScopes;
	}
	
}