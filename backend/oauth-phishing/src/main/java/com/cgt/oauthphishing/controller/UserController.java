package com.cgt.oauthphishing.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cgt.oauthphishing.dao.user.RoleRepository;
import com.cgt.oauthphishing.dao.user.UserRepository;
import com.cgt.oauthphishing.model.helpers.user.UserHelper;
import com.cgt.oauthphishing.model.tables.user.Role;
import com.cgt.oauthphishing.model.tables.user.User;
import com.cgt.oauthphishing.util.Logs;
import com.cgt.oauthphishing.util.tools.Tools;

import net.minidev.json.JSONObject;

/**
 * User data controller
 * */
@RestController
@RequestMapping(path="/user")
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private Logs logs;
	
	/**
	 * get authenticated user data
	 * */
	@GetMapping(path="/me")
	@ResponseBody 
	public User me(Principal principal){
		return (principal != null ? get(principal.getName()) : null);
	}
	
	/**
	 * get item by username
	 * */
	@Secured({"ROLE_ADMIN"})
	@GetMapping(path="/get")
	@ResponseBody 
	public User get(@RequestParam String username){
		return userRepository.findOneByEmail(username);
	}
	
	/**
	 * get encoded string
	 * */
	@Secured({"ROLE_ADMIN"})
	@GetMapping(path="/getEncoded")
	@ResponseBody 
	public JSONObject getEncoded(@RequestParam String input){		
		JSONObject response = new JSONObject();		
		String encoded = new BCryptPasswordEncoder().encode(input);		
		response.put("encodedString", encoded);		
		return response; 
	}
	
	/**
	 * save user (create or edit)
	 * */
	@Secured({"ROLE_ADMIN"})
	@PostMapping(path="/save")
	public ResponseEntity<JSONObject> save(@RequestBody UserHelper newUser){
		
		// validate input data
		if (newUser.getEmail() == null || newUser.getEmail().isEmpty())
			return Tools.jsonError("invalid email");
		if (newUser.getName() == null || newUser.getName().isEmpty())
			return Tools.jsonError("invalid name");
		
		Optional<User> posibleUser;
		User user;
		Optional<Role> posibleRole;
		Set<Role> roles = new HashSet<Role>();
		
		if (newUser.getId() != null) {
			// edit user
			posibleUser = userRepository.findById(newUser.getId());			
			if (posibleUser.isPresent()) user = posibleUser.get();
			else return Tools.jsonError("user not found");
		} else {
			// create user
			user = new User();
			
			// validate duplicated email
			if (userRepository.findOneByEmail(newUser.getEmail()) != null) {
				return Tools.jsonError("duplicated email");
			}
		}
		
		// add roles to user
		if (newUser.getRolesIds() != null && newUser.getRolesIds().length > 0) {
			for (int i = 0; i < newUser.getRolesIds().length; i++) {
				posibleRole = roleRepository.findById(newUser.getRolesIds()[i]);
				if (posibleRole.isPresent()) roles.add(posibleRole.get());
				else return Tools.jsonError("role with id: " + i + " not found");
			}
		} else {
			return Tools.jsonError("roles ids required");
		}
		
		user.setRoles(roles);
		
		// check password for new user
		if (newUser.getId() == null && newUser.getPassword() == null) {
			return Tools.jsonError("password is required for new user");
		}
		
		// add or edit password
		if (newUser.getPassword() != null) {
			user.setKey(newUser.getPassword());
		}
				
		user.setName(newUser.getName());
		user.setEmail(newUser.getEmail());
		user = userRepository.save(user);
		logs.save("Se han guardado datos de un usuario", user);

		return Tools.jsonSuccess(user);
		
	}
	
	/**
	 * change item status (enable or disable) 
	 * */
	@Secured({"ROLE_ADMIN"})
	@PostMapping(path="/changeStatus")
	public ResponseEntity<User> changeStatus(@RequestParam("username") String username, @RequestParam("enable") boolean enable){
		
		User user = userRepository.findOneByEmail(username);
		
		if (user != null) {
			
			user.setEnabled(enable);
			user = userRepository.save(user);

			logs.save("Se ha " + (enable ? "habilitado" : "deshabilitado") + " un usuario", user);
			return new ResponseEntity<User>(user, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
		}
		
	}
	
	/**
	 * get all items 
	 * */
	@Secured({"ROLE_ADMIN"})
	@GetMapping(path="/getAll")
	@ResponseBody 
	public Iterable<User> getAll(){		
		logs.save("Se han consultado todos los usuarios", null);
		return userRepository.findAll();
	}
	
	/* ROLES */
	
	/**
	 * get all roles
	 * */
	@GetMapping(path="/getAllRoles")
	@ResponseBody 
	public Iterable<Role> getAllRoles(){
		return roleRepository.findAll();
	}
	
}
