package com.cgt.oauthphishing.service.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.cgt.oauthphishing.model.helpers.mail.MailHelper;
import com.cgt.oauthphishing.util.constants.MailStrings;
import com.cgt.oauthphishing.util.tools.Tools;

import net.minidev.json.JSONObject;

/**
 * mail service
 * */
@Service
public class MailService {
	
	@Autowired
	private JavaMailSender sender;

	@Autowired
	private TemplateEngine templateEngine;
	
	@Value("${app.oauth2.oauth-uri-login}")
    private String fakeLoginUrl;

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * send general email
	 * */
	public ResponseEntity<JSONObject> sendGeneralEmail(MailHelper mail) {
		
		// validate input
		if (mail.getRecipients().length == 0 || mail.getSubject() == null || mail.getText() == null) {
			return Tools.jsonError("invalid mail body");
		}
		
		String text = mail.getText();
		mail.setText(text);
		
		boolean success = sendGeneral(mail);
		
		if (success) {
			return Tools.jsonSuccess(mail);
		} else {
			return Tools.jsonError("send mail error");
		}
	}
	
	/**
	 * send general email
	 * */
	private boolean sendGeneral(MailHelper mail) {
		
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper;
		String imageName = "main_logo";
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String copyrightText = "Copyright \u00a9 " + year + ", All rights reserved";
		
		try {
			
			// Prepare the evaluation context
			Context context = new Context();
	        context.setVariable("copyrightText", copyrightText);
	        context.setVariable("title", mail.getSubject());
	        context.setVariable("message", mail.getText());
	        context.setVariable("fakeLoginUrl", fakeLoginUrl);
	        context.setVariable("mainLogo", imageName);
			
	        // Prepare message using a Spring helper
			helper = new MimeMessageHelper(message, true, "UTF-8");
			helper.setSubject(mail.getSubject());
			helper.setFrom(MailStrings.MASTER_EMAIL);
			helper.setTo(mail.getRecipients());
			
			// Create the HTML body using Thymeleaf
			String content = templateEngine.process("generalMailTemplate", context);
			helper.setText(content, true);

		    // Add the inline image
			File file = new ClassPathResource("img/" + imageName + ".png").getFile();		    
		    final InputStreamSource imageSource = new ByteArrayResource(IOUtils.toByteArray(new FileInputStream(file)));
		    helper.addInline(imageName, imageSource, "image/png");
			
			// send message
		    try {
		    	sender.send(message);
			} catch (Exception e) {
				log.error("error sending email", e);
			}			
		    
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
