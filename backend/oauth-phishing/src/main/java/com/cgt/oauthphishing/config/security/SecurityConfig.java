package com.cgt.oauthphishing.config.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // @formatter:off
		auth.jdbcAuthentication()
			.passwordEncoder(new BCryptPasswordEncoder())
			.dataSource(dataSource).usersByUsernameQuery(
				"SELECT u.email as username, clave AS password, "
				+ "(CASE u.habilitado WHEN 1 THEN 'true' ELSE 'false' END) AS enabled "
				+ "FROM usuario u "
				+ "WHERE u.email = ?")
			.authoritiesByUsernameQuery(
				"SELECT u.email AS username, r.nombre AS role "
				+ "FROM usuario u "
				+ "INNER JOIN usuario_rol ur ON u.id = ur.Usuario_id "
				+ "INNER JOIN rol r ON ur.Rol_id = r.id "
				+ "WHERE u.email = ?")
			.rolePrefix("ROLE_");
	}

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
    	// @formatter:off
    	http.authorizeRequests()
	        .anyRequest().authenticated();
        // @formatter:on
    }
}

