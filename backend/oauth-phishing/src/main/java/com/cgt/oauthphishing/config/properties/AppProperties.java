package com.cgt.oauthphishing.config.properties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class for custom properties (can change in applications.yml file)
 * */
@ConfigurationProperties(prefix = "app")
public class AppProperties {
    private final OAuth2 oauth2 = new OAuth2();

    public static final class OAuth2 {

    	private String oauthUriLogin = new String();
    	private String redirectUriSuccess = new String();
    	private String redirectUriError = new String();

		public String getOauthUriLogin() {
			return oauthUriLogin;
		}

		public void setOauthUriLogin(String oauthUriLogin) {
			this.oauthUriLogin = oauthUriLogin;
		}

		public String getRedirectUriSuccess() {
			return redirectUriSuccess;
		}
		
		public void setRedirectUriSuccess(String redirectUriSuccess) {
			this.redirectUriSuccess = redirectUriSuccess;
		}
		
		public String getRedirectUriError() {
			return redirectUriError;
		}
		
		public void setRedirectUriError(String redirectUriError) {
			this.redirectUriError = redirectUriError;
		}
    	
    }

    public OAuth2 getOauth2() {
        return oauth2;
    }
}
