package com.cgt.oauthphishing.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cgt.oauthphishing.model.helpers.mail.MailHelper;
import com.cgt.oauthphishing.service.mail.MailService;
import com.cgt.oauthphishing.util.tools.Tools;

import net.minidev.json.JSONObject;

/**
 * General controller 
 * */
@Controller
public class GeneralController {

	@Autowired
	Environment env;
	
	@Autowired
	private MailService mailService;
	
	private static final String MAIL_SUBJECT = "Welcome to Xbox Game Pass";
	private static final String MAIL_TEXT = "With xbox game pass you can play video games from your Xbox or from Windows paying a monthly fee of € 1 forever!";
	
	/**
	 * get general logo preview
	 * @return image as byte array. If file not exist, return default logo in "resources/img/logo.png"
	 * */
	@GetMapping(path="/getMainLogo")
	@ResponseBody 
	public ResponseEntity<byte[]> getMainLogo() throws IOException{
		
		byte[] image;
		String logoPath = "img/logo.png";
		String rootPath = env.getProperty("paths.root");
	    MediaType mediaType = null;		
		ClassLoader classLoader = getClass().getClassLoader();
		
		try {

			File logoPNG = new File(rootPath + "logo.png");
			File logoJPEG = new File(rootPath + "logo.jpg");
			
			if (logoPNG.exists()) {
				mediaType = MediaType.IMAGE_PNG;
				image = IOUtils.toByteArray(new FileInputStream(logoPNG));
			} else if (logoJPEG.exists()){
				mediaType = MediaType.IMAGE_JPEG;
				image = IOUtils.toByteArray(new FileInputStream(logoJPEG));
			} else {
				mediaType = MediaType.IMAGE_JPEG;
				image = IOUtils.toByteArray(classLoader.getResourceAsStream(logoPath));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			mediaType = MediaType.IMAGE_JPEG;
			image = IOUtils.toByteArray(classLoader.getResourceAsStream(logoPath));
			return new ResponseEntity<byte[]>(image, HttpStatus.OK);
		}
	    
	    return ResponseEntity.ok().contentType(mediaType).body(image);
	}
	
	/**
	 * send mail
	 * */
	@Secured({"ROLE_ADMIN"})
	@PostMapping(path="/sendMail")
	public ResponseEntity<JSONObject> sendMail( 
		HttpServletRequest request, 
		@RequestParam("email") String email
	){
		// validate input
		if (email == null || email.isEmpty())
			return Tools.jsonError("Invalid mail");
        
		MailHelper mail = new MailHelper();
		
		mail.setRecipients(new String[] {email});
		mail.setSubject(MAIL_SUBJECT);
		mail.setText(MAIL_TEXT);
		
		return mailService.sendGeneralEmail(mail);		
	}
}
