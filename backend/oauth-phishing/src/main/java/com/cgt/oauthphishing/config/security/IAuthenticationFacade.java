package com.cgt.oauthphishing.config.security;

import org.springframework.security.core.Authentication;

/**
 * Interface for get authenticated user
 * */

public interface IAuthenticationFacade {

    Authentication getAuthentication();

}
