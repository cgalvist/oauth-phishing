package com.cgt.oauthphishing.model.tables.oauth;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_code")
public class OauthCode {

	@Id
	@Column(name = "code", length = 255)
	private String code;
	@Column(name = "authentication")
	private Blob authentication;

	public OauthCode(){}

	public OauthCode(String code, Blob authentication) {
		super();
		this.code = code;
		this.authentication = authentication;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Blob getAuthentication() {
		return authentication;
	}

	public void setAuthentication(Blob authentication) {
		this.authentication = authentication;
	}
	
}