package com.cgt.oauthphishing.model.tables.oauth;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_access_token")
public class OauthAccessToken {
	
	@Id
	@Column(name = "authentication_id", length = 255, unique = true, nullable = false)
	private String authenticationId;
	@Column(name = "token")
	private Blob token;
	@Column(name = "token_id", length = 255)
	private String tokenId;
	@Column(name = "user_name", length = 255)
	private String userName;
	@Column(name = "client_id", length = 255)
	private String clientId;
	@Column(name = "authentication")
	private Blob authentication;
	@Column(name = "refresh_token", length = 255)
	private String refresh_token;

	public OauthAccessToken(){}

	public OauthAccessToken(String authenticationId, Blob token, String userName, String clientId,
			Blob authentication, String refresh_token) {
		super();
		this.authenticationId = authenticationId;
		this.token = token;
		this.userName = userName;
		this.clientId = clientId;
		this.authentication = authentication;
		this.refresh_token = refresh_token;
	}

	public String getAuthenticationId() {
		return authenticationId;
	}

	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}

	public Blob getToken() {
		return token;
	}

	public void setToken(Blob token) {
		this.token = token;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Blob getAuthentication() {
		return authentication;
	}

	public void setAuthentication(Blob authentication) {
		this.authentication = authentication;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	
}