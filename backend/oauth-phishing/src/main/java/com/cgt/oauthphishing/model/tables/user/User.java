package com.cgt.oauthphishing.model.tables.user;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * User model class
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "usuario")
public class User implements java.io.Serializable {
	
	private Integer id;
	private String name;
	private String email;
	private boolean enabled = true;
	private String key;
	private Date creationDate;
	private Date editionDate;
	private Date birthDate;
	private Date lastLogin;

	private Set<Role> roles = new HashSet<Role>();
	
	public User(){		
	}
	
	public User(Integer id){
		this.id = id;
	}

	public User(Integer id, String name, String email, boolean enabled, String key, Set<Role> roles) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.enabled = enabled;
		this.key = key;
		this.roles = roles;
	}

	public User(User user){
		this.id = user.id;
		this.name = user.name;
		this.email = user.email;
		this.enabled = user.enabled;
		this.key = user.key;
		this.roles = user.roles;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "nombre", length = 100)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "email", length = 100, unique = true, nullable = false)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "habilitado")
	public boolean getEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@JsonIgnore
	@Column(name = "clave", length = 100, nullable = false)
	public String getKey() {
		return key;
	}
	
	public void setKey(String password) {
		this.key = password;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name="usuario_rol", joinColumns = {
			@JoinColumn(name = "usuario_id")
		}, 
		inverseJoinColumns = {
			@JoinColumn(name = "rol_id")
		}
	)
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Column(name = "fecha_creacion")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "fecha_edicion")
	public Date getEditionDate() {
		return editionDate;
	}

	public void setEditionDate(Date editionDate) {
		this.editionDate = editionDate;
	}

	@Column(name = "fecha_nacimiento")
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Column(name = "ultima_sesion")
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	@PrePersist
	protected void onCreate() {
		this.creationDate = new Date();
		this.enabled = false;
	}

	@PreUpdate
	protected void onUpdate() {
		this.editionDate = new Date();
	}

}
