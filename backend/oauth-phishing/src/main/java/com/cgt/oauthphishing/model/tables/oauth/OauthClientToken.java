package com.cgt.oauthphishing.model.tables.oauth;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_client_token")
public class OauthClientToken {

	@Id
	@Column(name = "authentication_id", length = 255, unique = true, nullable = false)
	private String authenticationId;
	@Column(name = "token_id", length = 255)
	private String tokenId;
	@Column(name = "token")
	private Blob token;
	@Column(name = "user_name", length = 255)
	private String userName;
	@Column(name = "client_id", length = 255)
	private String clientId;
	
	public OauthClientToken(){}

	public OauthClientToken(String authenticationId, String tokenId, Blob token, String userName, String clientId) {
		super();
		this.authenticationId = authenticationId;
		this.tokenId = tokenId;
		this.token = token;
		this.userName = userName;
		this.clientId = clientId;
	}

	public String getAuthenticationId() {
		return authenticationId;
	}

	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public Blob getToken() {
		return token;
	}

	public void setToken(Blob token) {
		this.token = token;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

}