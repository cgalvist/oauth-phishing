package com.cgt.oauthphishing.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.cgt.oauthphishing.OauthPhishingApplication;;

/**
 * class for configure project for make deployable WAR
 * */
public class ServletInitializer extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OauthPhishingApplication.class);
	}
	
}
