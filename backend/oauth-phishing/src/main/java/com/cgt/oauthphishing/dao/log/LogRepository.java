package com.cgt.oauthphishing.dao.log;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cgt.oauthphishing.model.tables.log.Log;

public interface LogRepository extends CrudRepository<Log, Long> {
	
	@Query("select l from Log l "
			+ "where (?1 is null or l.username like CONCAT('%',?1,'%')) "
			+ "and ((?2 is null or l.date >= ?2) and (?3 is null or l.date <= ?3))")
	public List<Log> getAll(String username, Date startDate, Date endDate, Pageable pageable);
	
}