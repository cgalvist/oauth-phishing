package com.cgt.oauthphishing.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.cgt.oauthphishing.config.security.IAuthenticationFacade;
import com.cgt.oauthphishing.dao.log.LogRepository;
import com.cgt.oauthphishing.model.tables.log.Log;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class with logs functions
 * */
@Service
public class Logs {
	
	@Autowired
    private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	private LogRepository logRepository;
	
    private final Logger logger = LoggerFactory.getLogger(Logs.class);	
	
	/**
	 * save log in database
	 * @param description is the log description
	 * @param objectData is the Java object data for cast as JSON
	 * */
	public void save(String description, Object objectData){
		
		Authentication authentication = authenticationFacade.getAuthentication();
		
		Log log = new Log();
		log.setUsername(authentication.getName());
		log.setDescription(description);
		
		if (objectData != null) {
			ObjectMapper mapper = new ObjectMapper();
			String data;
			try {
				data = mapper.writeValueAsString(objectData);
				log.setData(data);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		
		log = logRepository.save(log);
		
		logger.debug("save log with data:\n "
				+ "-user: " + log.getUsername()
				+ "-description: " + log.getDescription());
	}

}
