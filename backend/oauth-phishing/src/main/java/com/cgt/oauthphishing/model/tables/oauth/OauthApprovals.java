package com.cgt.oauthphishing.model.tables.oauth;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "oauth_approvals")
public class OauthApprovals implements Serializable {
	
	@Id
	@Column(name = "code", length = 255)
	private String userId;	
	@Id
	@Column(name = "code", length = 255)
	private String clientId;	
	@Id
	@Column(name = "code", length = 255)
	private String scope;	
	@Id
	@Column(name = "code", length = 10)
	private String status;	
	@Id
	@Column(name = "expiresAt")
	private Date expiresAt;	
	@Id
	@Column(name = "lastModifiedAt")
	private Date lastModifiedAt;
	
	@PreUpdate
	protected void onUpdate() {
		lastModifiedAt = new Date();
	}
	
	public OauthApprovals(){}

	public OauthApprovals(String userId, String clientId, String scope, String status, Date expiresAt,
			Date lastModifiedAt) {
		super();
		this.userId = userId;
		this.clientId = clientId;
		this.scope = scope;
		this.status = status;
		this.expiresAt = expiresAt;
		this.lastModifiedAt = lastModifiedAt;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
	}

	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}

	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	
}