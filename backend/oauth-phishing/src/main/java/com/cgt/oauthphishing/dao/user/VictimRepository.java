package com.cgt.oauthphishing.dao.user;

import org.springframework.data.repository.CrudRepository;

import com.cgt.oauthphishing.model.tables.user.Victim;

public interface VictimRepository extends CrudRepository<Victim, Integer> {

	public Victim findOneByEmail(String email);
	
}
