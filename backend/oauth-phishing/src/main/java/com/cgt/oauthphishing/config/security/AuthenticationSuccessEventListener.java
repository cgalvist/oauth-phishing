package com.cgt.oauthphishing.config.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import com.cgt.oauthphishing.service.security.LoginAttemptService;

/**
 * class for detect success login
 * */
@Component
public class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent> {
	
	private final Logger log = LoggerFactory.getLogger(AuthenticationSuccessEventListener.class);
	
    @Autowired
    private LoginAttemptService loginAttemptService;

	public void onApplicationEvent(AuthenticationSuccessEvent event) {

		String username = event.getAuthentication().getName();
		
		log.debug("login success: " + username);
		loginAttemptService.loginSucceeded(username);
	}
}
