# Back End
Creado con Spring Boot 1.5.4 y MySQL 5.7.18


## Instalación (entorno de desarrollo)

* Verificar si todos los proyectos del back end tienen el archivo ".classpath" y ".project" en la raiz. En caso de no estar estos archivos, haga una copia de los mismos en las respectivas carpetas:

.classpath
```xml
<?xml version="1.0" encoding="UTF-8"?>
<classpath>
	<classpathentry kind="src" output="target/classes" path="src/main/java">
		<attributes>
			<attribute name="optional" value="true"/>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry excluding="**" kind="src" output="target/classes" path="src/main/resources">
		<attributes>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry kind="src" output="target/test-classes" path="src/test/java">
		<attributes>
			<attribute name="optional" value="true"/>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry kind="con" path="org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER">
		<attributes>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry kind="output" path="target/classes"/>
</classpath>
```

.project
```xml
<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
  <name>CAMBIAR-NOMBRE</name>
  <comment></comment>
  <projects></projects>
  <buildSpec>
    <buildCommand>
    <name>org.eclipse.jdt.core.javabuilder</name>
    <arguments></arguments>
    </buildCommand>
  </buildSpec>
  <natures>
    <nature>org.eclipse.jem.workbench.JavaEMFNature</nature>
  	<nature>org.eclipse.wst.common.modulecore.ModuleCoreNature</nature>
  	<nature>org.springframework.ide.eclipse.core.springnature</nature>
  	<nature>org.eclipse.jdt.core.javanature</nature>
  	<nature>org.eclipse.m2e.core.maven2Nature</nature>
  	<nature>org.eclipse.wst.common.project.facet.core.nature</nature>
  	<nature>org.eclipse.wst.jsdt.core.jsNature</nature>
  </natures>
</projectDescription>
```

* Abrir STS (Eclipse) configurando el Workspace en la carpeta "backend" del repositorio.
* En la sección "Package Explorer", importar el proyecto dando click derecho en "Import" -> "General" -> "Existing Projects into Workspace". Luego seleccionar los proyectos de Spring y dar click en "Finish".
* Se exportarán los proyectos con errores. Para solucionarlos dar click derecho en el proyecto, luego en "Maven" -> "Update Project" -> "Ok".
* Por último, ejecutar dando click derecho en el proyecto y seleccionar "Run As" -> "Spring Boot App" (configurar primero el perfil de ejecución).

## Perfiles de ejecución

Al crear un WAR o desplegar la aplicación, se deben utilizar diferentes perfiles de Spring Boot para configuraciones en diferentes entornos (por ejemplo, una configuración específica para el entorno de producción y otra para el entorno de desarrollo).  
La configuración de perfiles se encuentra en el archivo "application.yml" ubicada en "src/main/resources".  

Al ejecutar en Eclipse, puede utilizar un perfil ingresando a "run" -> "run configurations..." y luego escribir el nombre del perfil deseado en el campo "Profile".  

* Para entornos de desarrollo con Linux, debe escribir los perfiles `develop`

## Base de datos

* Es necesario que exista una base de datos con el nombre "oauth_phishing":
```shell
mysql -u root -p
```
```sql
CREATE DATABASE oauth_phishing;
```

* Para el entorno de desarrollo se requiere en MySQL un usuario llamado "demo" con contraseña "demo". Puede hacerlo en MySQL con las siguientes órdenes.

```sql
CREATE USER 'demo'@'localhost' IDENTIFIED BY 'demo';  
GRANT ALL PRIVILEGES ON * . * TO 'demo'@'localhost';  
FLUSH PRIVILEGES;
```

* Para construir la base de datos, puede ejecutar el script "Database.sql" ubicado en "files/develop/backend/":
```shell
mysql -u root -p oauth_phishing < Database.sql
```

* También puede construir la base de datos (solo en estructura) cambiando la configuración del backend "spring.jpa.hibernate.ddl-auto" con el valor "create". USAR CON CUIDADO.
 
* Para realizar una copia de seguridad de la base de datos, puede ejecutar la seguiente orden, ubicado en "files/develop/backend/":
```shell
mysqldump -u root -p oauth_phishing > Database.sql
```

## Configuración de la aplicación OAuth

Estos fueron los servicios de Microsoft Graph agregados en la aplicación

![alt text][logo]

[logo]: images/app_config_1.png "Logo Title Text 2"

Este desarrollo se ha realizado en base a las siguientes páginas:
* [Configuración sencilla de back end con Spring Boot](https://www.devglan.com/spring-security/spring-boot-security-google-oauth)
* [Configuración avanzada de back end con Spring Boot (dividida en 3 partes)](https://www.callicoder.com/spring-boot-security-oauth2-social-login-part-1/)
